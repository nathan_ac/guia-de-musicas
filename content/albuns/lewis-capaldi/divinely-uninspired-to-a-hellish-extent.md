---
title: "Divinely Uninspired to a Hellish Extent"
date: 2021-10-24T21:36:09-03:00
draft: false
---

# Grace:
`I'm not ready to be just another of your mistakes`
---
`I can't seem to drown you out long enough`
---
`I fell victim to the sound of your love`
---
`You're like a song that I ain't ready to stop`
---
`I got nothing but you on my mind`
---
`I'm not ready to be just another of your mistakes`
---
`Don't wanna let the pieces fall out of place`
---
`I was only just a breath removed from going to waste`
---
`'Till I found salvation in the form of your`
---
`Your grace`
---
`Your grace`
---
`Your grace`
---
`Don't take it away`
---
`Your grace`
---
`Your grace`
---
`Your grace`
---
`Don't take it away`
---
`On the verge of almost bleeding you out`
---
`Are we too wounded now to ever come down?`
---
`Oh, how I long for us to find common ground`
---
`I got nothing but you on my mind`
---
`I'm not ready to be just another of your mistakes`
---
`Don't wanna let the pieces fall out of place`
---
`I was only just a breath removed from going to waste`
---
`'Till I found salvation in the form of your`
---
`Your grace`
---
`Your grace`
---
`Your grace`
---
`Don't take it away`
---
`Your grace`
---
`Your grace`
---
`Your grace`
---
`Don't take it, take it`
---
`Way too close to colour your comfort`
---
`All dressed up, but kept undercover`
---
`Way too close to colour your comfort`
---
`All dressed up, but kept undercover`
---
`Your grace`
---
`Your grace`
---
`Your grace`
---
`Don't take it away`
---
`I'm not ready to be just another of your mistakes`
---
`Don't wanna let the pieces fall out of place`
---
`I was only just a breath removed from going to waste`
---
`'Till I found salvation in the form of your`
---
`Your grace (your grace)`
---
`Your grace (your grace)`
---
`Your grace (your grace)`
---
`Don't take it away`
---
`Your grace (your grace)`
---
`Your grace (your grace)`
---
`Your grace`
---
`Don't take it away`
---
---
---
# Someone You Loved:
---
`I'm going under and this time`
---
`I fear there's no one to save me`
---
`This all or nothing really got a way`
---
`Of driving me crazy`
---
`I need somebody to heal, somebody to know`
---
`Somebody to have, somebody to hold`
---
`It's easy to say, but it's never the same`
---
`I guess I kinda liked`
---
`The way you numbed all the pain`
---
`Now the day bleeds into nightfall`
---
`And you're not here`
---
`To get me through it all`
---
`I let my guard down`
---
`And then you pulled the rug`
---
`I was getting kinda used`
---
`To being someone you loved`
---
`I'm going under and this time`
---
`I fear there's no one to turn to`
---
`This all or nothing way of loving`
---
`Got me sleeping without you`
---
`Now, I need somebody to know, somebody to heal`
---
`Somebody to have, just to know how it feels`
---
`It's easy to say, but it's never the same`
---
`I guess I kinda liked`
---
`The way you helped me escape`
---
`Now the day bleeds into nightfall`
---
`And you're not here`
---
`To get me through it all`
---
`I let my guard down`
---
`And then you pulled the rug`
---
`I was getting kinda used`
---
`To being someone you loved`
---
`And I tend to close my eyes`
---
`When it hurts sometimes`
---
`I fall into your arms`
---
`I'll be safe in your sound`
---
`till I come back around`
---
`For now the day bleeds into nightfall`
---
`And you're not here`
---
`To get me through it all`
---
`I let my guard down`
---
`And then you pulled the rug`
---
`I was getting kinda used`
---
`To being someone you loved`
---
`But now the day bleeds into nightfall`
---
`And you're not here`
---
`To get me through it all`
---
`I let my guard down`
---
`And then you pulled the rug`
---
`I was getting kinda used`
---
`To being someone you loved`
---
`I let my guard down`
---
`And then you pulled the rug`
---
`I was getting kinda used`
---
`To being someone you loved`
---