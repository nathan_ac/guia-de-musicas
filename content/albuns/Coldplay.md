---
title: "Coldplay"
date: 2021-10-24T12:24:24-03:00
draft: true
---

[A Rush of Blood to the Head]({{< relref path="albuns/coldplay/A-Rush-of-Blood-to-the-Head.md">}})
[Parachutes]({{< relref path="albuns/coldplay/Parachutes.md">}})
[X&Y]({{< relref path="albuns/coldplay/XY.md">}})
