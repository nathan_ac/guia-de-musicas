---
title: "XY"
date: 2021-10-24T13:02:51-03:00
draft: false
---
# A Message:
---
`My song is love`
---
`Love to the loveless shown`
---
`And it goes up`
---
`You don't have to be alone`
---
`Your heavy heart`
---
`Is made of stone`
---
`And it's so hard to see clearly`
---
`You don't have to be on your own`
---
`You don't have to be on your own`
---
`And I'm not gonna take it back`
---
`And I'm not gonna say I don't mean that`
---
`You're the target that I'm aiming at`
---
`And I'll get that message home`
---
`My song is love`
---
`My song is love unknown`
---
`And I'm on fire for you clearly`
---
`You don't have to be alone`
---
`You don't have to be on your own`
---
`And I'm not gonna take it back`
---
`And I'm not gonna say I don't mean that`
---
`You're the target that I'm aiming at`
---
`And I'm nothing on my own`
---
`Got to get that message home`
---
`And I'm not gonna stand and wait`
---
`Not gonna leave it until it's much too late`
---
`On a platform, I'm gonna stand and say`
---
`That I'm nothing on my own`
---
`And I love you please come home`
---
`My song is love is love unknown`
---
`And I've got to get that message home`
---
---
---
# What if:
---
`What if there was no lie?`
---
`Nothing wrong, nothing right`
---
`What if there was no time?`
---
`And no reason, or rhyme`
---
`What if you should decide`
---
`That you don't want me there by your side`
---
`That you don't want me there in your life`
---
`What if I got it wrong`
---
`And no poem or song`
---
`Could put right what I got wrong`
---
`Or make you feel I belong`
---
`What if you should decide`
---
`That you don't want me there by your side`
---
`That you don't want me there in you life`
---
`Ooh, that's right`
---
`Let's take a breath, jump over the side`
---
`Ooh, that's right`
---
`How can you know it if you don't even try?`
---
`Ooh, that's right`
---
`Every step that you take`
---
`Could be your biggest mistake`
---
`It could bend or it could break`
---
`But that's the risk that you take`
---
`What if you should decide`
---
`That you don't want me there in your life`
---
`That you don't want me there by your side`
---
`Ooh, that's right`
---
`Let's take a breath, jump over the side`
---
`Ooh, that's right`
---
`How can you know when you don't even try?`
---
`Ooh, that's right`
---
`Oh`
---
`Ooh, that's right`
---
`Let's take a breath, jump over the side`
---
`Ooh, that's right`
---
`You know that darkness always turns into light`
---
`Ooh, that's right`
---