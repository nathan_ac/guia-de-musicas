---
title: "A Rush of Blood to the Head"
date: 2021-10-24T13:02:18-03:00
draft: false
---

# In My Place:
`In My Place, In My Place`
---
`Were lines that I couldn't change`
---
`I was lost, oh yeah`
---
`I was lost, I was lost`
---
`Crossed lines I shouldn't have crossed`
---
`I was lost, oh yeah`
---
`Yeah`
---
`How long must you wait for it?`
---
`Yeah`
---
`How long must you pay for it?`
---
`Yeah`
---
`How long must you wait for it?`
---
`Oh, for it`
---
`I was scared, I was scared`
---
`Tired and under prepared`
---
`But I'll wait for it`
---
`If you go, if you go`
---
`Leave me down here on my own`
---
`Then I'll wait for you (yeah)`
---
`Yeah`
---
`How long must you wait for it?`
---
`Yeah`
---
`How long must you pay for it?`
---
`Yeah`
---
`How long must you wait for it?`
---
`Oh, for it?`
---
`Singing please, please, please`
---
`Come back and sing to me`
---
`To me, me`
---
`Come on and sing it out`
---
`Now, now`
---
`Come on and sing it out`
---
`To me, me`
---
`Come back and sing`
---
`In My Place, In My Place`
---
`Were lines that I couldn't change`
---
`And I was lost, oh yeah`
---
`Oh yeah`
---
---
---
# The Scientist:
---
`Come up to meet you, tell you I'm sorry`
---
`You don't know how lovely you are`
---
`I had to find you, tell you I need you`
---
`Tell you I set you apart`
---
`Tell me your secrets and ask me your questions`
---
`Oh, let's go back to the start`
---
`Running in circles, coming up tails`
---
`Heads on a science apart`
---
`Nobody said it was easy`
---
`It's such a shame for us to part`
---
`Nobody said it was easy`
---
`No one ever said it would be this hard`
---
`Oh, take me back to the start`
---
`I was just guessing at numbers and figures`
---
`Pulling the puzzles apart`
---
`Questions of science, science and progress`
---
`Do not speak as loud as my heart`
---
`But tell me you love me, come back and haunt me`
---
`Oh and I rush to the start`
---
`Running in circles, chasing our tails`
---
`Coming back as we are`
---
`Nobody said it was easy`
---
`Oh, it's such a shame for us to part`
---
`Nobody said it was easy`
---
`No one ever said it would be so hard`
---
`I'm going back to the start`
---
`Oh ooh, ooh ooh ooh ooh`
---
`Ah ooh, ooh ooh ooh ooh`
---
`Oh ooh, ooh ooh ooh ooh`
---
`Oh ooh, ooh ooh ooh ooh`
---