---
title: "Parachutes"
date: 2021-10-24T13:01:37-03:00
draft: false
---

# We Never Change:
---
`I want to live life and never be cruel`
---
`And I want to live life and be good to you`
---
`And I want to fly and never come down`
---
`And live my life and have friends around`
---
`We never change, do we?`
---
`No, no`
---
`We never learn, do we?`
---
`So I want to live in a wooden house`
---
`I want to live life and always be true`
---
`And I want to live life and be good to you`
---
`And I want to fly and never come down`
---
`And live my life and have friends around`
---
`We never change, do we?`
---
`No, no`
---
`We never learn, do we?`
---
`So I want to live in a wooden house`
---
`Where making more friends would be easy`
---
`Oh, and I don't have a soul to save`
---
`Yes, and I sin every single day`
---
`We never change, do we?`
---
`We never learn, do we?`
---
`So I want to live in a wooden house`
---
`Where making more friends would be easy`
---
`I want to live where the sun comes out`
---
---
---
# Parachutes:
`In a haze, a stormy haze`
---
`I'll be round I'll be loving you always`
---
`Always`
---
`Here, I am and I'll take my time`
---
`Here, I am and I'll wait in the line always`
---
`Always`
---
